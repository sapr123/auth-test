'use strict'

const PassSservice = require('./services/pass.service');
const moment = require('moment');
const { hash } = require('bcrypt');

// Datos para simulacion
const miPass = "miContraseña";
const badPass = "miOtraConstraseña";
const usuario = {
    _id: '123456789',
    email: 'steevenpr02@gmail.com',
    displayName: 'Steeven Pereira',
    password: miPass,
    signUpdate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

// Encriptamos el password...
PassSservice.encriptaPassword( usuario.password).then( hash => {
    usuario.password = hash;
    console.log(usuario);

    // Verificamos el pass
    PassSservice.comparaPassword(miPass, usuario.password).then(isOk => {
        if(isOk){
            console.log('p1: El pass es correcto');
        } else{
            console.log('p1: El pass no es correcto');
        }
    }).catch(err => console.log(err));

    // Verificamos el pass contra un pass falso
    PassSservice.comparaPassword(badPass, usuario.password).then(isOk => {
        if(isOk){
            console.log('p2: El pass es correcto');
        } else{
            console.log('p2: El pass no es correcto');
        }
    }).catch(err => console.log(err));

});