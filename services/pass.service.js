'use strict'

const bcrypt = require('bcrypt');

// encriptaPassword
//
// Formato del hash:
//  $2b$10$Xs6EgN3KG3FstZneQYzaCOG.HelBUSSGLhlPrZO0Jbrp1I/LSZL52
//  ****-- **********************+++++++++++++++++++++++++++++++
//  Alg Cost        Salt                      Hash
//

function encriptaPassword( password){
    return bcrypt.hash(password, 10);
}

// comparaPassword
//
// Devolver verdadero o falso si coinciden o no el pass y hash
//
 function comparaPassword(password, hash) {
     return bcrypt.compare(password, hash);
 }

 module.exports = {
     encriptaPassword,
     comparaPassword
 };